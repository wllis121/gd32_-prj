/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU SD
  * @version V1.0
  * @date    26-Dec-2014
  * @brief   The main function file.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gd32f10x.h"
#include <stdio.h>
#include "systick.h"
#include "gd32f10x_usart.h"


/* Private function prototypes -----------------------------------------------*/
static void usart_config(void);

/* Private variables ---------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
    SysTick_Configuration();
    
    /* configure USART0 */
    usart_config();

    while (1)
    {
        printf("Hello Usart1 test!\n");
        Delay_1ms(500);
    }
}

/*!
    \brief      usart configure
    \param[in]  none
    \param[out] none
    \retval     none
*/
static void usart_config(void)
{ 
    /* enable GPIO clock */
    /* Enable GPIOA clock */
    RCC_APB2PeriphClock_Enable( RCC_APB2PERIPH_GPIOA  , ENABLE );
    
    /* Enable USART1 APB clock */
    RCC_APB2PeriphClock_Enable( RCC_APB2PERIPH_USART1  , ENABLE );

    /* Configure the GPIO ports */
    GPIO_InitPara GPIO_InitStructure;
   
    GPIO_InitStructure.GPIO_Pin     = GPIO_PIN_9 ;
    GPIO_InitStructure.GPIO_Mode    = GPIO_MODE_AF_PP;
    GPIO_InitStructure.GPIO_Speed   = GPIO_SPEED_50MHZ;
    GPIO_Init( GPIOA , &GPIO_InitStructure); 
    GPIO_InitStructure.GPIO_Pin     = GPIO_PIN_10;
    GPIO_InitStructure.GPIO_Mode    = GPIO_MODE_IN_FLOATING;;
    GPIO_Init( GPIOA , &GPIO_InitStructure); 

    /* USART configure */
    USART_InitPara USART_InitStructure;
    
    USART_DeInit( USART1 );
    USART_InitStructure.USART_BRR           = 115200;
    USART_InitStructure.USART_WL            = USART_WL_8B;
    USART_InitStructure.USART_STBits            = USART_STBITS_1;
    USART_InitStructure.USART_Parity                = USART_PARITY_RESET;
    USART_InitStructure.USART_HardwareFlowControl = USART_HARDWAREFLOWCONTROL_NONE;
    USART_InitStructure.USART_RxorTx                = USART_RXORTX_RX | USART_RXORTX_TX;
    USART_Init(USART1, &USART_InitStructure);
    
        /* USART enable */
    USART_Enable(USART1, ENABLE);
}
/**
  * @brief  Retarget the C library printf function to the USART.
  * @param  None
  * @retval None
  */
int fputc(int ch, FILE *f)
{
    /* Loop until transmit data register is empty */
    while (USART_GetBitState( USART1 , USART_FLAG_TBE) == RESET)
    {
    }
    /* Place your implementation of fputc here */
    USART_DataSend( USART1 , (uint8_t) ch );
    return ch;
}
/******************* (C) COPYRIGHT 2014 GIGADEVICE *****END OF FILE****/ 

