/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU SD
  * @version V1.0
  * @date    26-Dec-2014
  * @brief   The main function file.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gd32f10x.h"
#include <stdio.h>
#include "systick.h"

/* Private function prototypes -----------------------------------------------*/
void LED_GPIO_Configuration(void);
void Turn_On_LED(uint8_t LED_NUM);

/* Private variables ---------------------------------------------------------*/
uint8_t  count=0;

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
    LED_GPIO_Configuration();
    SysTick_Configuration();

    while (1)
    {
        GPIO_ResetBits(GPIOB, GPIO_PIN_0|GPIO_PIN_1);
        Turn_On_LED(count%4);
        count++;
        Delay_1ms(100);
    }
}

/**
  * @brief  Configure the LED GPIO ports.
  * @param  None
  * @retval None
  */
void LED_GPIO_Configuration(void)
{
    GPIO_InitPara GPIO_InitStructure;

    /* Enable GPIOC clock */
    RCC_APB2PeriphClock_Enable(RCC_APB2PERIPH_GPIOB, ENABLE);

    GPIO_InitStructure.GPIO_Pin   = GPIO_PIN_0|GPIO_PIN_1;
    GPIO_InitStructure.GPIO_Speed = GPIO_SPEED_50MHZ;
    GPIO_InitStructure.GPIO_Mode  = GPIO_MODE_OUT_PP;
    GPIO_Init(GPIOB,&GPIO_InitStructure);
    
    GPIO_ResetBits(GPIOB, GPIO_PIN_0|GPIO_PIN_1);
}

/**
  * @brief  Light the LEDs.
  * @param  LED_NUM��LEDx where x can be 2..5.
  * @retval None
  */
void Turn_On_LED(uint8_t LED_NUM)
{
    switch(LED_NUM)
    {
        /* Light the LED2 */
        case 0:
            GPIO_SetBits(GPIOB,GPIO_PIN_0);
        break;

        /* Light the LED3 */
        case 1:
            GPIO_SetBits(GPIOB,GPIO_PIN_1);
        break;

        /* Light the LED4 */
        case 2:
            GPIO_SetBits(GPIOB,GPIO_PIN_0);
        break;

        /* Light the LED5 */
        case 3:
            GPIO_SetBits(GPIOB,GPIO_PIN_1);
        break;

        /* Light all the LEDs */
        default:
            GPIO_SetBits(GPIOB, GPIO_PIN_0|GPIO_PIN_1);
        break;
    }
}

/******************* (C) COPYRIGHT 2014 GIGADEVICE *****END OF FILE****/ 

