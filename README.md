# GD32_Prj

 **软件平台：** Keil5  
 **Keil版本：** 5.27.0.0  
 **pack包：** GD32F10x_AddOn_V2.0.2.rar  
 **芯片型号：** GD32F103C8T6  

#### 1、GPIO_Running_LED  
GD32F103C8T6实现两个LED灯闪烁例子，LED引脚对应PB0,PB1  

#### 2、USART1_Send  
GD32F103C8T6实现USART1串口发送例子，串口1引脚PA9(TX),PA10(RX)